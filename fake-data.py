#!/usr/bin/env python3

from faker import Faker
import time
import json
import hashlib

fake = Faker()

sources = []
pages = []

for i in range(fake.pyint(5, 50)):
    name = fake.catch_phrase()
    shelfmark = fake.ssn()
    mintime = fake.past_datetime()
    added = time.mktime(mintime.timetuple())
    edited = time.mktime(fake.past_datetime(mintime).timetuple())
    sourcedata = {"name": name, "shelfmark": shelfmark, "added": added, "edited": edited}
    datajson = json.dumps(sourcedata)
    sourcesha = hashlib.sha1(datajson.encode("UTF-8")).hexdigest()
    sourcedata['id'] = sourcesha
    for j in range(1, fake.pyint(20, 200)):
        name = str(j)
        source = sourcesha
        pagemintime = fake.past_datetime(mintime)
        added = time.mktime(pagemintime.timetuple())
        edited = time.mktime(fake.past_datetime(pagemintime).timetuple())
        pagedata = {"name": name, "source": source, "added": added, "edited": edited}
        datajson = json.dumps(pagedata)
        pagesha = hashlib.sha1(datajson.encode("UTF-8")).hexdigest()
        pagedata['id'] = pagesha
        pages.append(pagedata)
    sources.append(sourcedata)
myjson = json.dumps({"sources": sources, "pages": pages}, sort_keys=True, indent=4)

print(myjson)
