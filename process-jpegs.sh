#!/bin/sh
temp_file="$(mktemp)"
orig_file="${TAR_FILENAME}"
output_dir="src/to-your-rude-health-data"
orig_dir="${output_dir}/$(dirname ${orig_file})"
mkdir -p "$orig_dir"
cat > "$temp_file"
if expr "$(file -b $temp_file)" : 'JPEG' > /dev/null; then
	cwebp -resize 832 0 -q 50 -m 6 "$temp_file" -o "${output_dir}/${orig_file%.*}.webp"
	convert "$temp_file" -strip -thumbnail 832 -quality 50 "${output_dir}/${orig_file%.*}.jpg"
else
	cp "$temp_file" "${output_dir}/$orig_file"
fi
rm -f "$temp_file"
