# To Your Rude Health!

This is the website and static site generator for the *To Your Rude Health!* project.

```
npm install
npm run build
```

Gitlab CI/CD requires these variables:

- `OUTPUT_DOMAIN`: The domain that the pages will be hosted on.
- `OUTPUT_SUBDIR`: The subdirectory of the pages.

These variables are used to generate absolute URLs for the project and RDF especially.

This project is licensed under AGPL3.
