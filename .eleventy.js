module.exports = function(eleventyConfig) {
	var config = require('./src/_data/config.json');
	const { URL } = require('url');
	const { parse } = require('node-html-parser');
	const markdownIt = require('markdown-it');
	const markdownItAttrs = require('markdown-it-attrs');
	const htmlmin = require("html-minifier");
	/* Pass through - stop eleventy touching */
	if(config.images) {
		eleventyConfig.addPassthroughCopy({'src/to-your-rude-health-data/images': 'images'});
		eleventyConfig.addPassthroughCopy({'src/to-your-rude-health-data/audio': 'audios'});
		eleventyConfig.addPassthroughCopy({'src/to-your-rude-health-data/pdf': 'pdf'});
		eleventyConfig.addPassthroughCopy({'src/images/static': 'images/static'});
	}
	eleventyConfig.addPassthroughCopy({
		'src/solid.png': 'solid.png',
		'src/ifei.woff': 'ifei.woff',
		'src/ifer.woff': 'ifer.woff',
		'src/satisfy.woff': 'satisfy.woff',
		'src/.htaccess': '.htaccess'
	});
	eleventyConfig.setFrontMatterParsingOptions({
		excerpt: true,
		excerpt_separator: "<!-- excerpt -->"
	});
	eleventyConfig.addTransform("htmlmin", function(content, outputPath) {
		if(outputPath && outputPath.endsWith(".html")) {
			let minified = htmlmin.minify(content, {
				useShortDoctype: true,
				removeComments: true,
				collapseWhitespace: true
			});
			return minified;
		}

		return content;
	});
	repl = function(i) {
		if(typeof i == 'string') {
			return i.replace(/[^0-9a-z ]/gi, '')
				.toLowerCase()
				.trim()
				.replace(/^(the\s+)+(.+)/, '$2$1')
				.replace(/^(a\s+)+(.+)/, '$2$1');
		}
		return i;
	};
	keysig_split = function(s) {
		if(typeof s !== "string") {
			return {
				"tonic_key": null,
				"tonic_acc": null,
				"mode": null
			};
		}
		const spl = (s.match(/([A-Z])(-flat|-sharp)?( major| minor)?/i) ?? []).slice(1);
		let acc = spl[1];
		let mode = spl[2];
		if(typeof acc === "string") {
			if(acc === "-flat") {
				acc = "b";
			} else if(acc === "-sharp") {
				acc = "s";
			}
		} else {
			acc = undefined;
		}
		if(typeof mode === "string") {
			mode = mode.trim();
		}
		return {
			"tonic_key": spl[0],
			"tonic_acc": acc,
			"mode": mode
		};
	};
	eleventyConfig.addFilter("extract_tonic", function(str) {
		const splitted = keysig_split(str);
		if(!splitted.tonic_key) {
			throw "Could not extract tonic from: " + str;
			return null;
		}
		return splitted.tonic_key + (splitted.tonic_acc || "");
	});
	eleventyConfig.addFilter("extract_mode", function(str) {
		const splitted = keysig_split(str);
		if(!splitted.mode) {
			throw "Could not extract mode from: " + str;
			return null;
		}
		return splitted.mode;
	});
	eleventyConfig.addFilter("extract_beats", function(str) {
		const lower = str.toLowerCase();
		if(lower.includes("common")) {
			return 4;
		}
		if(lower.includes("cut")) {
			return 2;
		}
		const beats = parseInt(lower);
		if(!beats) {
			throw "Could not extract beats from: " + str;
			return null;
		}
		return beats;
	});
	eleventyConfig.addFilter("extract_beattype", function(str) {
		const lower = str.toLowerCase();
		if(lower.includes("common")) {
			return 4;
		}
		if(lower.includes("cut")) {
			return 2;
		}
		const match = parseInt((lower.match(/([0-9]+)\/([0-9]+)/))[2]);
		if(!match) {
			throw "Could not extract beat type from: " + str;
			return null;
		}
		return match;
	});
	eleventyConfig.addFilter("abs_url", function(str) {
		if(!config.domain) {
			throw "To generate absolute url we require a base domain";
			return str;
		}
		if(config.abs_url) {
			const abs = new URL(str, config.domain);
			return abs.href;
		}
		return str;
	});
	eleventyConfig.addFilter("alphasplit", function(arr, attr = 'name') {
		let splititems = [];
		let currentalpha = '';
		let currentlist = [];
		arr.forEach(function(item) {
			if(!item || !(attr in item)) {
				return;
			}
			let thisfirst = ''
			if(item[attr]) {
				thisfirst = repl(item[attr]).charAt(0).toUpperCase();
			}
			if(thisfirst != currentalpha) {
				currentalpha = thisfirst;
				if(currentlist.length != 0) {
					splititems.push(currentlist);
					currentlist = [];
				}
			}
			currentlist.push(item);
		});
		if(currentlist.length != 0) {
			splititems.push(currentlist);
		}
		return splititems;
	});
	eleventyConfig.addFilter("firstletter", function(str) {
		return repl(str).charAt(0).toUpperCase();
	});
	eleventyConfig.addFilter("bettersort", function(arr, attr = 'name') {
		return arr.sort(function(a, b) {
			test = function(t) { return (typeof t === "object" && (attr in t)); };
			if(!test(a)) {
				return -1;
			}
			if(!test(b)) {
				return 1;
			}
			a = repl(a[attr]);
			b = repl(b[attr]);
			return a.localeCompare(b);
		});
	});
	eleventyConfig.addFilter("trimnonalpha", function(str) {
		if(typeof str !== 'string') {
			return str;
		}
		return str.replace(/\s?\W$/, '');
	});
	eleventyConfig.addFilter("numericsort", function(arr, attr = false) {
		return arr.sort(function(b, a) {
			const test = function(t) { return (typeof t === "object" && (attr in t)); };
			const numcmp = function(a, b) {
				const makenum = function(i) { return i.match(/\d+/g).map(Number); };
				const ais = makenum(a).reverse();
				const bis = makenum(b).reverse();
				let ai, bi;
				while((ai = ais.pop()) !== undefined && (bi = bis.pop()) !== undefined) {
					const diff = bi - ai;
					if(diff !== 0) {
						return diff;
					}
				}
				const lendiff = b.length - a.length;
				if(lendiff !== 0) {
					return lendiff;
				}
				return a.localeCompare(b);
			};
			if(!test(a)) {
				if(!test(b)) {
					return numcmp(a, b);
				}
				return numcmp(a, b[attr]);
			}
			if(!test(b)) {
				return numcmp(a[attr], b);
			}
			return numcmp(a[attr], b[attr]);
		});
	});
	eleventyConfig.addFilter("attrdive", function(arr, attr) {
		return [].concat.apply([], arr.map(function(e) { return e[attr]; }));
	});
	eleventyConfig.addFilter("sortLinkLen", function(arr, attr, reverse = false) {
		sortfun = function(a, b) {
			attra = a.links[attr];
			attrb = b.links[attr];
			return (attrb.length - attra.length);
		}
		if(reverse) {
			return arr.sort(function(a, b) {
				return sortfun(b, a);
			});
		}
		return arr.sort(sortfun);
	});
	eleventyConfig.addFilter("unique", function(arr) {
		return arr.filter(function(v, i, s) { return s.indexOf(v) === i; });
	});
	eleventyConfig.addFilter("selectattrhas", function(arr, attr, val) {
		return arr.filter(function(v, i, s) {
			return (Array.isArray(v[attr]) && v[attr].includes(val));
		});
	});
	eleventyConfig.addFilter("prefixURLs", function(html, prefix) {
		const root = parse(html);
		const items = root.querySelectorAll('[src],[href]');
		for(const item of items) {
			const srcAttribute = item.getAttribute('src');
			const hrefAttribute = item.getAttribute('href');
			const processAttribute = (attr) => {
				if(attr.includes("://")) {
					return attr;
				}
				return ("/" + prefix + "/" + attr).replace(/\/+/g, "/");
			};
			if(srcAttribute) {
				item.setAttribute('src', processAttribute(srcAttribute));
			}
			if(hrefAttribute) {
				item.setAttribute('href', processAttribute(hrefAttribute));
			}
		}
		return root.toString();
	});
	const EnglishNumber = require('english-number');
	eleventyConfig.addFilter("englishNumber", function(num) {
		return EnglishNumber.nameOf(num);
	});
	const smartquotes = require('smartquotes');
	eleventyConfig.addFilter("smartquotes", function(str) {
		return smartquotes(String(str));
	});
	eleventyConfig.addFilter("forID", function(str) {
		const b = new Buffer.from(str);
		return b.toString('base64').replace(/=/g, '');
	});
	eleventyConfig.addFilter("toHTML", function(str) {
		return markdownIt({html:true,breaks:false}).renderInline(str);
	});
	const markdownLib = markdownIt({html:true}).use(markdownItAttrs);
	eleventyConfig.setLibrary("md", markdownLib);
	let ret_obj = {
		dir: { input: 'src', output: 'dist', data: '_data' },
		passthroughFileCopy: true,
		templateFormats: ['njk',  'md', 'css', 'html', 'yml', '11ty.js'],
		htmlTemplateEngine: 'njk'
	}
	if(config.subdir) {
		ret_obj.pathPrefix = config.subdir;
	}
	return ret_obj;
}
