---
layout: layouts/static.njk
title: About
---

*To Your Rude Health* is a collaborative project involving students ([Joshua
Stutter](https://www.gla.ac.uk/pgrs/joshuastutter/), Alasdair Robertson and
[Edward Marshall](https://www.gla.ac.uk/pgrs/edwardmarshall/)) and staff ([Dr
Katy Lavinia Cooper](https://katylaviniacooper.weebly.com/), [Mr Neil
McDermott](http://neilmcdermott.co.uk/)) from the University of Glasgow.
<!-- excerpt -->
The music we have recorded is held in the University library’s Special
Collections, and the songs were recorded in the University’s Humanity Lecture
Theatre and Memorial Chapel. This project is a University of Glasgow project
through and through with the hope to bring to life music in the University
library that has previously been unfairly forgotten. We trust that this project
will attract more interest in the excellent music holdings of the library and
Special Collections, and inspire others to take on such projects using these
resources.

These recordings, editions and commentary are intended to be useful to all
those that study the music and culture of this period and provide a
collaborative platform upon which to base further study and music–making. We
know from our previous performances of this programme that the music, although
hundreds of years old, is still extremely accessible and enjoyable for general
audiences. Therefore, we aim for this musical resources to be used by amateur
and professional musicians alike for continuing performance, thereby bringing
the music of this somewhat forgotten repertory, once a common music heard in
inns and taverns, to a wide publich audience. In February 2019, we gave a free
performance of the programme of songs in [Special
Collections](https://www.gla.ac.uk/myglasgow/archivespecialcollections/) with
the sources available for viewing, which around 40 people attended, including
students, staff, as well as members of the public. In this way, we have already
seen the potential impact that this project can provide.

## Euing Collection

> The Euing collection contains over 2,500 volumes of early printed music,
> including 7 incunabula. The collection’s musical treasures include manuscripts
> of two medieval liturgical works, an early 17th century lute book, a set of
> part-books belonging to the publisher John Playford, and biographical
> documents and letters sent to John Sainsbury for use in his Dictionary of
> musicians which appeared in 1824. Published music includes contemporary
> editions of works by Byrd, Purcell, Gibbons, Lully, Marenzio, Couperin,
> Frescobaldi and many others, while among the theoretical works are several
> rare items, such as Thomas Morley’s Plaine and easie introduction to
> practicall musicke (1597) and Thomas Mace’s Musick’s monument (1676).

(Taken from the [University of Glasgow
website](https://www.gla.ac.uk/myglasgow/specialcollections/collectionsa-z/euingcollection/),
where can be found more information about the collection as well as a detailed
breakdown of its holdings.)

## Chancellor’s Fund

Recording *To Your Rude Health*, digitising the sources and developing this web
resource has been generously funded by the University of Glasgow’s [Chancellor’s
Fund](https://www.gla.ac.uk/connect/supportus/chancellorsfund/). This project
would simply not have been possible without this support and we are deeply
grateful.

## Iuchair

[Iuchair](https://yokermusic.scot/?iuchair) is an early music vocal ensemble
based in Glasgow, brought together by a shared interest in early music,
particularly that of neglected, unusual and seldom–performed repertories. Always
performing from bespoke editions, Iuchair put particular emphasis on bringing
performance of early music up to date with current academic thinking by
dispelling myths of early music performance. From our foundation in September
2017, we have considered foremost how the music is best performed in the modern
day against current conceptions of how Medieval music “should” sound, especially
in terms of pitch, tuning and tempo. The three projects that we have worked on
so far are Perotin: the Scottish Source - 13th Century polyphony from W1 (St
Andrews), To your rude health / Sacred and Profane: 17th Century Catches and
Drinking Songs from sources held in the Euing collection at the University of
Glasgow, Special Collections, and Passio: music for Holy Week by the gentlemen
of the Chapel Royal. 

[facebook.com/Iuchair](https://www.facebook.com/Iuchair)

[@iuchair](https://twitter.com/iuchair)

# How to Cite

Stutter, J., Marshall, E., Robertson, A., Cooper, K., McDermott, N., & Campbell, H. (2021). “To Your Rude Health!”.

# Credits

- Recordings by *Iuchair*: Edward Marshall, Alasdair Robertson, Joshua Stutter
- Producers: Katy Lavinia Cooper & Neil McDermott
- Engineer: Neil McDermott
- Facsimiles: Edward Marshall & Harry Campbell
- Website Design: Joshua Stutter
- All images of books reproduced courtesy of University of Glasgow Archives & Special Collections

# Special Thanks

- Julie Gardham and the whole Special Collections team for supporting the
  project
- [Dr Tim Duguid](https://www.gla.ac.uk/schools/humanities/staff/timduguid/) for
  his RDF advice

<a href="https://www.gla.ac.uk"><img src="/images/static/uofglogo.png" alt="University of Glasgow logo" style="margin:auto;display:block;"/></a>
