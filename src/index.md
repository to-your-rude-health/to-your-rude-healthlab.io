---
layout: layouts/homepage.njk
title: Welcome,
---

[Iuchair Ensemble](https://yokermusic.scot/?iuchair) have recently recorded our
programme of seventeenth– and eighteenth–century catches and drinking songs held
in the University of Glasgow’s [Euing
collection](https://www.gla.ac.uk/myglasgow/specialcollections/collectionsa-z/euingcollection/) *To Your Rude Health*.
<!-- excerpt -->
We have developed this web resource in order to make the audio recordings
available online alongside bespoke scholarly editions of the songs, facsimile
reproductions of the sources, as well as commentary and information about the
Euing collection. Few of these songs have been recorded before. We hope that
you enjoy the recordings and find this resource useful.

This project was supported by the University of Glasgow Archives and Special
Collections, and funded by the University of Glasgow Chancellor's Fund.

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D431166920755817&show_text=0&width=832" width="832" height="468" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
