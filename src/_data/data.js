var raw = require('../to-your-rude-health-data/tyrh.json');
var config = require('./config.json');
var pluralize = require('pluralize');

function isFunction(fun) {
	return fun && {}.toString.call(fun) === '[object Function]';
}

for(const person of raw.people) {
	const split = person.name.split(" ");
	person.surname = split[split.length - 1];
}
const punctregex = /(?![!"#$%&()*+,./:;<=>?@[^_`{|}~£\]\-\n])/
const maxlen = typeof config.truncate != 'undefined' ? config.truncate : 64;
for(const text of raw.texts) {
	const split = (text.edited.split(punctregex)).reduce(function(acc, curr) {
		if(typeof acc == 'string') {
			acc = [acc];
		}
		acc[acc.length - 1] += curr;
		if(curr.length != 1) {
			acc.push([]);
		}
		return acc;
	}).reverse();
	let result = "";
	while(split.length > 0 && (result.length + split[split.length - 1].length) <= maxlen) {
		result += split.pop();
	}
	result = result.replace(/\s?\W$/, '');
	if(result.length > 0 && result.length < text.edited.length) {
		text.truncated = result + "‥";
	}
}
let voices = [];
for(const item of raw.items) {
	const split = (item.presentedtext.split(punctregex)).reduce(function(acc, curr) {
		if(typeof acc == 'string') {
			acc = [acc];
		}
		acc[acc.length - 1] += curr;
		if(curr.length != 1) {
			acc.push([]);
		}
		return acc;
	}).reverse();
	let result = "";
	while(split.length > 0 && (result.length + split[split.length - 1].length) <= maxlen) {
		result += split.pop();
	}
	result = result.replace(/\s?\W$/, '');
	if(result.length > 0 && result.length < item.presentedtext.length) {
		item.presentedtexttruncated = result + "‥";
	}
	if(item.voices) {
		item.voices = parseInt(item.voices);
		const vrecord = voices.find(function(x) { return x.voices === item.voices; });
		if(vrecord) {
			vrecord.item_id.push(item.id);
		} else {
			let new_vrecord = {
				"voices": item.voices,
				"item_id": [item.id]
			};
			voices.push(new_vrecord);
		}
	}
}
raw.voices = voices;
const ik = Object.keys(raw);
// Composers and editors are people too!
raw.composers = function() { return this.people; };
raw.editors = function() { return this.people; };
for(const i of ik) {
	const cat = raw[i];
	if(!Array.isArray(cat)) {
		debugger;
	}
	for(const j of cat) {
		const ij = Object.keys(j);
		for(const k of ij) {
			if(k.endsWith('_id')) {
				lca = k.substring(0, k.length - 3);
				lcat = pluralize.plural(lca);
				ids = j[k];
				j[lcat] = [];
				for(const id of ids) {
					var found = false
					if(isFunction(raw[lcat])) {
						found = raw[lcat]().find(x => x.id == id);
					} else {
						found = raw[lcat].find(x => x.id == id);
					}
					if(found) {
						j[lcat].push(found)
						if(!found.links) {
							found.links = {};
						}
						if(!found.links[i]) {
							found.links[i] = [];
						}
						found.links[i].push(j);
					}
				}
			}
		}
	}
}
function replace_whitespace(i) {
	return i.replace(/\s+/g, ' ');
}
for(let item of raw.items) {
	let sortabletitle = item.presentedtext;
	if(item.title) {
		sortabletitle = item.title;
	} else if(item.links.songs[0].texts[0].edited) {
		sortabletitle = item.links.songs[0].texts[0].edited;
	}
	item.sortabletitle = replace_whitespace(sortabletitle);
}
for(let song of raw.songs) {
	song.sortabletitle = replace_whitespace(song.texts[0].edited);
	let keys = ['composers', 'voices', 'keys', 'timesigs', 'tempos', 'pages'];
	for(const key of keys) {
		song[key] = [];
	}
	for(const item of song.items) {
		for(const key of keys) {
			song[key] = song[key].concat(item[key]);
		}
	}
	for(const key of keys) {
		song[key] = song[key].filter((item, pos) => song[key].indexOf(item) === pos);
	}
	for(const key of keys) {
		let iterate = song[key];
		if(key == 'voices') {
			iterate = [];
			for(const voice of song.voices) {
				iterate.push(raw.voices.find(function(x) { return x.voices === voice; }));
			}
		}
		for(let item of iterate) {
			if(!item.links) {
				item.links = {};
			}
			if(!item.links.songs) {
				item.links.songs = [];
			}
			if(item.links.songs.indexOf(song) == -1) {
				item.links.songs.push(song);
			}
		}
	}
}
raw.images = [];
const image_keys = ['sources', 'pages']
for(const key of image_keys) {
	for(const cat of raw[key]) {
		for(const image of cat.images) {
			let irecord = raw.images.find(function(x) { return x.filename === image; });
			if(irecord === undefined) {
				irecord = {
					"filename": image,
					"links": { }
				};
				irecord.links[key] = [cat];
				raw.images.push(irecord);
			} else {
				if(!irecord.links[key]) {
					irecord.links[key] = [cat];
				} else {
					irecord.links[key].push(cat);
				}
			}
		}
	}
}
module.exports = raw;
