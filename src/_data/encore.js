const fs = require('fs');

module.exports = function() {
	try {
		const data = fs.readFileSync('./dist/javascript/entrypoints.json', 'utf8');
		const entrypoints = JSON.parse(data);
		return entrypoints.entrypoints;
	} catch(e) {
		console.log("Looks like the files do not exist, wait for them to be built!");
	}
};
