import "core-js/stable";
import "regenerator-runtime/runtime";
import Fuse from 'fuse.js';
import dom from 'jsx-render';
import gtag, { install } from 'ga-gtag';
import Cookies from 'js-cookie';
import URL from 'url-parse';
const options = {
	keys: [
		"name",
		"pagenum",
		"composer",
		"genre",
		"key",
		"text",
		"tempo",
		"timesig",
		"title",
		"voices"
	], 
	minlength: 3,
	maxresults: 5,
	updatetime: 1000,
};
const processFuncs = {
	"Character": function(item) {
		return (<a href={item.permalink}><div class="search-result result-character">
			<span>Character</span>
			<span>
				<p><b>{item.name}</b></p>
			</span>
		</div></a>);
	},
	"Genre": function(item) {
		return (<a href={item.permalink}><div class="search-result result-genre">
			<span>Genre</span>
			<span>
				<p><b>{item.name}</b></p>
			</span>
		</div></a>);
	},
	"Item": function(item) {
		let titular = item.text;
		if(item.title) {
			titular = item.title;
			return (<a href={item.permalink}><div class="search-result result-item">
				<span>Song</span>
				<span>
					<p><b>{titular}</b></p>
					<p><i>{item.composer}</i></p>
					<p style="font-size:.75rem">{item.text}</p>
				</span>
			</div></a>);
		}
		return (<a href={item.permalink}><div class="search-result result-item">
			<span>Song</span>
			<span>
				<p><b>{titular}</b></p>
				<p><i>{item.composer}</i></p>
			</span>
		</div></a>);
	},
	"Key": function(item) {
		return (<a href={item.permalink}><div class="search-result result-key">
			<span>Key</span>
			<span>
				<p><b>{item.name}</b></p>
			</span>
		</div></a>);
	},
	"Page": function(item) {
		return (<a href={item.permalink}><div class="search-result result-page">
			<span>Page</span>
			<span>
				<p><b>{item.pagenum}</b></p>
			</span>
		</div></a>);
	},
	"Person": function(item) {
		return (<a href={item.permalink}><div class="search-result result-person">
			<span>Person</span>
			<span>
				<p><b>{item.name}</b></p>
			</span>
		</div></a>);
	},
	"Song": function(item) {
		return (<a href={item.permalink}><div class="search-result result-song">
			<span>Song</span>
			<span>
				<p><b>{item.text}</b></p>
				<p><i>{item.composer}</i></p>
			</span>
		</div></a>);
	},
	"Tempo": function(item) {
		return (<a href={item.permalink}><div class="search-result result-tempo">
			<span>Tempo</span>
			<span>
				<p><b>{item.name}</b></p>
			</span>
		</div></a>);
	},
	"Text": function(item) {
		return (<a href={item.permalink}><div class="search-result result-text">
			<span>Text</span>
			<span>
				<p>{item.text}</p>
			</span>
		</div></a>);
	},
	"Time Signature": function(item) {
		return (<a href={item.permalink}><div class="search-result result-timesig">
			<span>Time Signature</span>
			<span>
				<p><b>{item.name}</b></p>
			</span>
		</div></a>);
	},
	"Number of Voices": function(item) {
		return (<a href={item.permalink}><div class="search-result result-voice">
			<span>Number of Voices</span>
			<span>
				<p><b>{item.voices}</b></p>
			</span>
		</div></a>);
	}
};
document.addEventListener('DOMContentLoaded', () => {
	const documentFile = ("/" + configSubdir + "/documents.json").replace(/\/+/g, "/");
	fetch(documentFile)
		.then(response => response.json())
		.then(function(documents) {
			const fuse = new Fuse(documents, options);
			const search_div = document.getElementById('search');
			const search_input = document.getElementById('search-input');
			const search_results = document.getElementById('search-results');
			const change_func = function(evt) {
				if(search_input.value.length < options.minlength) {
					return;
				}
				const results = fuse.search(search_input.value);
				if(results.length > options.maxresults) {
					results.length = options.maxresults;
				}
				search_results.innerHTML = '';
				if(results.length === 0) {
					search_results.style.display = 'none';
				} else {
					for(const result of results) {
						const cat = result.item.category;
						if(!(cat in processFuncs)) {
							continue;
						}
						const res = processFuncs[cat](result.item);
						search_results.appendChild(res);
					}
				}
				search_results.style.display = 'block';
			};
			search_div.style.display = 'block';
			for(const evt of ['keyup', 'focus', 'change']) {
				search_input.addEventListener(evt, change_func);
			}
			search_input.addEventListener('blur', function() {
				if(!search_results.matches(':hover')) {
					search_results.style.display = 'none';
				}
			});
		});
	document.getElementsByClassName('no-js').forEach((el) => {
		el.style.display = 'none';
	});
	document.getElementsByClassName('js').forEach((el) => {
		el.style.display = 'initial';
	});
	document.getElementsByTagName('audio').forEach((el) => {
		el.addEventListener('play', () => {
			document.getElementsByTagName('audio').forEach((other) => {
				if(other !== el) {
					other.pause();
				}
			});
		});
	});

	const cookieName = 'tyrh-track';
	const noMoreCookie = () => {
		document.getElementsByClassName('cookie-consent').forEach((el) => {
			el.style.display = 'none';
		});
	};
	const loadGA = () => {
		install('G-BPVWPZ6L5Y', {'send_page_view': true});
	};
	const refHost = document.referrer !== '' ? URL(document.referrer).host : null;
	const thisHost = URL(window.location.href).host;
	console.log("Referrer host is:", refHost);
	console.log("Host is:", thisHost);
	if(refHost === thisHost || Cookies.get(cookieName) === true) {
		noMoreCookie();
	} else {
		document.getElementById('cookie-accept').addEventListener('click', () => {
			console.log("Cookie accept");
			Cookies.set(cookieName, true, {sameSite: 'strict'});
			noMoreCookie();
			loadGA();
		});
		document.getElementById('cookie-reject').addEventListener('click', () => {
			console.log("Cookie reject");
			noMoreCookie();
		});
	}
});
