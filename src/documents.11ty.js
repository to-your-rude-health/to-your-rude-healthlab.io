const smartquotes = require("smartquotes");
class Documents {
	data() {
		return {
			permalink: "documents.json",
			eleventyExcludeFromCollections: true
		};
	}
	quote_if_not_null(thing) {
		return (thing == null ? thing : smartquotes(String(thing)));
	}
	render(pdata) {
		const data = pdata.data
		let items = [
			...data.characters.map(this.generate_character.bind(this)),
			...data.genres.map(this.generate_genre.bind(this)),
			...data.items.map(this.generate_item.bind(this)),
			...data.keys.map(this.generate_key.bind(this)),
			...data.pages.map(this.generate_page.bind(this)),
			...data.people.map(this.generate_person.bind(this)),
			...data.songs.map(this.generate_song.bind(this)),
			...data.tempos.map(this.generate_tempo.bind(this)),
			...data.texts.map(this.generate_text.bind(this)),
			...data.timesigs.map(this.generate_timesig.bind(this)),
			...data.voices.map(this.generate_voice.bind(this))
		];
		return JSON.stringify(items);
	}
	generate_character(item) {
		return {
			"category": "Character",
			"name": this.quote_if_not_null(item.name),
			"permalink": this.url("/characters/" + item.id + "/")
		};
	}
	generate_genre(item) {
		return {
			"category": "Genre",
			"name": this.quote_if_not_null(item.name),
			"permalink": this.url("/genres/" + item.id + "/")
		};
	}
	generate_item(item) {
		return {
			"category": "Item",
			"composer": this.quote_if_not_null(item.presentedcomposer),
			"text": this.quote_if_not_null(item.presentedtext),
			"permalink": this.url("/items/" + item.id + "/")
		};
	}
	generate_key(item) {
		return {
			"category": "Key",
			"name": this.quote_if_not_null(item.name),
			"permalink": this.url("/keys/" + item.id + "/")
		};
	}
	generate_page(item) {
		return {
			"category": "Page",
			"pagenum": this.quote_if_not_null(item.pagenum),
			"permalink": this.url("/pages/" + item.id + "/")
		};
	}
	generate_person(item) {
		return {
			"category": "Person",
			"name": this.quote_if_not_null(item.name),
			"permalink": this.url("/people/" + item.id + "/")
		};
	}
	generate_song(item) {
		return {
			"category": "Song",
			"composer": item.composers.length == 0 ? null : this.quote_if_not_null(item.composers[0].name),
			"genre": item.genres.map((function(a) { return this.quote_if_not_null(a.name); }).bind(this)).join(", "),
			"key": item.keys.map((function(a) { return this.quote_if_not_null(a.name); }).bind(this)).join(", "),
			"text": item.texts.length == 0 ? null : this.quote_if_not_null(item.texts[0].edited),
			"tempo": item.tempos.map((function(a) { return this.quote_if_not_null(a.name); }).bind(this)).join(", "),
			"timesig": item.timesigs.map((function(a) { return this.quote_if_not_null(a.name); }).bind(this)).join(", "),
			"voices": this.quote_if_not_null(item.voices),
			"permalink": this.url("/songs/" + item.id + "/")
		};
	}
	generate_tempo(item) {
		return {
			"category": "Tempo",
			"name": this.quote_if_not_null(item.name),
			"permalink": this.url("/tempos/" + item.id + "/")
		};
	}
	generate_text(item) {
		return {
			"category": "Text",
			"text": this.quote_if_not_null(item.edited),
			"permalink": this.url("/texts/" + item.id + "/")
		};
	}
	generate_timesig(item) {
		return {
			"category": "Time Signature",
			"name": this.quote_if_not_null(item.name),
			"permalink": this.url("/timesigs/" + item.id + "/")
		};
	}
	generate_voice(item) {
		return {
			"category": "Number of Voices",
			"voices": this.quote_if_not_null(item.voices),
			"permalink": this.url("/voices/" + item.voices + "/")
		};
	}
}
module.exports = Documents;
