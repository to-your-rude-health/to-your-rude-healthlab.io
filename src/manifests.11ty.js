const { URL } = require('url');
const probe = require('probe-image-size');
const Keyv = require('keyv');
class Manifests {
	constructor() {
		this.keyv = new Keyv('sqlite://image-size-cache.db');
	}
	data() {
		return {
			pagination: {
				data: "data.images",
				size: 1,
				alias: "image"
			},
			eleventyExcludeFromCollections: true,
			permalink: data => `manifests/${data.image.filename}/manifest.json`
		};
	}
	async render({data, image, config}) {
		if(image.links.sources) {
			return await this.render_source(image.links.sources[0], image.filename, data, config);
		}
		if(image.links.pages) {
			const source = image.links.pages[0].sources[0];
			return await this.render_source(source, image.filename, data, config);
		}
		throw new Error("This image (" + image.filename + ") does not l"
			+ "ink to sources or pages.");
	}
	async render_source(source, filename, data, config) {
		const [items, start_id] = await this.render_items(source, filename, data, config);
		const base = new URL(config.subdir + "/manifests/" + filename +
			"/manifest.json", config.domain).toString();
		return JSON.stringify({
			"@context": "http://iiif.io/api/presentation/3/context.json",
			"id": base,
			"type": "Manifest",
			"label": {
				"en": [source.title]
			},
			"start": {
				"id": start_id,
				"type": "Canvas"
			},
			"items": items
		});
	}
	async image_size(url) {
		return await this.keyv.get(url).then((val) => {
			if(val === undefined) {
				return probe(url).then((resp) => {
					return this.keyv.set(url, resp).then((bool) => {
						return resp;
					});
				});
			}
			return val;
		});
	}
	async render_item(filename, basedir, config, label) {
		const full_image_size = (filename) => {
			const url = new URL(filename, config.fullImage).toString();
			return this.image_size(url);
		};
		const image_id = new URL(basedir + "/" + filename + "/canvas").toString();
		const annotation_page_id = new URL(basedir + "/" + filename + "/page").toString();
		const annotation_id = new URL(basedir + "/" + filename + "/annotation").toString();
		const placeholder_canvas_id = new URL(basedir + "/" + filename + "/placeholderCanvas").toString();
		const placeholder_annotation_page_id = new URL(basedir + "/" + filename + "/placeholderAnnotationPage").toString();
		const placeholder_annotation_id= new URL(basedir + "/" + filename + "/placeholderAnnotation").toString();
		const thumb_url = new URL("/images/" + filename, config.domain + "/" + config.subdir).toString();
		return full_image_size(filename).then((full_info) => {
			const scale = full_info.width / 832;
			const thumb_info = {
				"width": (full_info.width < 832 ? full_info.width : 832),
				"height": (full_info.width < 832 ? full_info.height : (full_info.height / scale)),
				"mime": full_info.mime,
				"url": thumb_url
			};
			return {
				"id": image_id,
				"type": "Canvas",
				"label": {
					"en": [label]
				},
				"rendering": full_info.url,
				"width": full_info.width,
				"height": full_info.height,
				"items": [
					{
						"id": annotation_page_id,
						"type": "AnnotationPage",
						"items": [
							{
								"id": annotation_id,
								"type": "Annotation",
								"motivation": "painting",
								"body": {
									"id": full_info.url,
									"type": "Image",
									"format": full_info.mime,
									"height": full_info.height,
									"width": full_info.width
								},
								"target": image_id
							}
						]
					}
				],
				"placeholderCanvas": {
					"id": placeholder_canvas_id,
					"type": "Canvas",
					"width": thumb_info.width,
					"height": thumb_info.height,
					"items": [
						{
							"id": placeholder_annotation_page_id,
							"type": "AnnotationPage",
							"items": [
								{
									"id": placeholder_annotation_id,
									"type": "Annotation",
									"motivation": "painting",
									"body": {
										"id": thumb_info.url,
										"type": "Image",
										"format": thumb_info.mime,
										"height": thumb_info.height,
										"width": thumb_info.width
									},
									"target": placeholder_canvas_id
								}
							]
						}
					]
				}
			};
		});
	}
	async render_items(source, filename, data, config) {
		let ret = [];
		const pages = this.numericsort(source.links.pages, 'pagenum');
		const other_images = source.images;
		const basedir = new URL('/manifests/' + filename, config.domain + "/" + config.subdir).toString();
		let start = null;
		for(const image of other_images) {
			const rendered = await this.render_item(image, basedir, config, "Other matter");
			if(image === filename) {
				start = rendered.id;
			}
			ret.push(rendered);
		}
		for(const page of pages) {
			for(const image of page.images) {
				const rendered = await this.render_item(image, basedir, config, "Page " + page.pagenum);
				if(image === filename) {
					start = rendered.id;
				}
				ret.push(rendered);
			}
		}
		return [ret, start];
	}
}
module.exports = Manifests;
