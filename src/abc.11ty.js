class ABC {
	data() {
		return {
			pagination: {
				data: "data.editions",
				size: 1,
				alias: "edition"
			},
			permalink: data => `abc/${data.edition.id}.abc`,
			eleventyExcludeFromCollections: true
		};
	}
	render({data, edition, config}) {
		return edition.abc;
	}
}
module.exports = ABC;
